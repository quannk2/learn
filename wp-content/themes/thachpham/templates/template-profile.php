<?php /*template name: profile */ ?>
<?php get_header(); ?>
<?php 
    $user = wp_get_current_user();
    $user_id = $user->ID;
?>
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
    <a href="<?php echo wp_logout_url(); ?>">Logout</a>
  </div>
  <?php
    if( isset( $_POST['user_profile_nonce_field'] ) && wp_verify_nonce( $_POST['user_profile_nonce_field'], 'user_profile_nonce' ) ) {
        if(!empty($_POST['nickname'])){
          update_user_meta($user_id, 'nickname',$_POST['nickname'] );
        }
        if(!empty($_POST['email'])){
          update_user_meta($user_id, 'user_email',$_POST['email'] );
        }
        if(!empty($_POST['first_name'])){
          update_user_meta($user_id, 'first_name',$_POST['first_name'] );
        }
        if(!empty($_POST['last_name'])){
          update_user_meta($user_id, 'last_name',$_POST['last_name'] );
        }
        if ( !empty($_POST['Pass1'] ) && !empty( $_POST['Pass2'] ) ) {
                if ( $_POST['Pass1'] == $_POST['Pass2'] )
                    wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['Pass1'] ) ) );
            }
    }

  ?>
  <div class="user_info">
    <div class="avatar" style="float: left; margin-right: 15px;">
      <?php  echo get_avatar( $user_id, 150 ); ?>
    </div>
    <div class="info">
        <p>Nick Name: <?php the_author_meta('nickname',$user_id); ?></p>
        <p>Email: <?php the_author_meta('user_email',$user_id); ?></p>
        <p>First Name: <?php the_author_meta('first_name',$user_id); ?></p>
        <p>Last Name: <?php the_author_meta('last_name',$user_id); ?></p>
    </div>
  </div>
  <!-- /.lockscreen-item -->
  <div class="edit_info">
    <input type="button" name="" value="Change profile" id="edit_profile">
  </div>
  <div class="text-center">
    <a href="<?php echo home_url().'/login/' ;?>">Or sign in as a different user</a>
  </div>
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2014-2018 <b><a href="http://adminlte.io" class="text-black">AdminLTE.io</a></b><br>
    All rights reserved
  </div>
</div>
<div id="edit">
    <form action="" id="user_profile" method="POST">
      <?php wp_nonce_field('user_profile_nonce', 'user_profile_nonce_field'); ?>
      <div>
        <label>Nick Name:</label>
        <input type="text" name="nickname">
      </div>
      <div>
        <label>Email:</label>
        <input type="email" name="email">
      </div>
      <div>
        <label>First Name:</label>
        <input type="text" name="first_name">
      </div>
      <div>
        <label>Last Name:</label>
        <input type="text" name="last_name">
      </div>
      <div>
        <label>Password:</label>
        <input type="password" name="Pass1">
      </div>
      <div>
        <label>Retype password:</label>
        <input type="password" name="Pass2">
      </div>
      <div style="margin-top: 10px">
        <input type="submit" name="Save_change" value="Save Change" style="width: 115px">
        <input type="button" id="cancel" value="cancel" style="width: 115px; border-radius: 5px; border: 1px solid gray;">
      </div>

    </form>
</div>
<!-- /.center -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<?php get_footer();?>